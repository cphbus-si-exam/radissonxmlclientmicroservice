using System;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.AspNetCore.Mvc;
using Models;

namespace RadissonXMLClient.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AccommodationController : Controller
    {
        [HttpGet]
        public IActionResult Get(){
            return Ok("Radisson XML Accommodation Client Is Alive");            
        }

        [HttpPost]
        public async Task<JsonResult> Post([FromBody]accommodationbooking accData){
            var httpClient = new HttpClient();
            var data = new StringContent(SerializeToString<accommodationbooking>(accData),Encoding.UTF8, "application/xml");
            var response = await httpClient.PostAsync(Environment.GetEnvironmentVariable("RADISON")+"/accommodation", data);

            var XMLrespmsg = response.Content.ReadAsStringAsync().Result;
            var serializer = new XmlSerializer(typeof(AccBookingModelRes));
            StringReader rdr = new StringReader(XMLrespmsg);
            var model = (AccBookingModelRes)serializer.Deserialize(rdr);
            return Json(model);
        }
        public string SerializeToString<T>(T value)
        {
            var emptyNamespaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
            var serializer = new XmlSerializer(value.GetType());
            var settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.OmitXmlDeclaration = true;

            using (var stream = new StringWriter())
            using (var writer = XmlWriter.Create(stream, settings))
            {
                serializer.Serialize(writer, value, emptyNamespaces);
                return stream.ToString();
            }
        }
    }
}