namespace Models
{
    public class accommodationbooking
    {
        public int numberofrooms { get; set; }
        public string startdate { get; set; }
        public string enddate { get; set; }
        public string eventid { get; set; }
        public string request_type { get; set; }
    }
}